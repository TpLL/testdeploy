// var list = document.querySelectorAll('div');

// for (var i = 0; i < list.length; i++) {
//     list[i].onclick = function (e) {
//         console.log(e.target.innerText);
//     };
// }
fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => {
        return response.json(); //JSON.parse: JSON -> javascript types
    })
    .then((post) => {
        var html = post.map(e => {
            return `
            <li>
            <h2>${e.title}</h2>
            <p>${e.body}</p>
            </li>`
        })
        html = html.join('');
        Document.getElementById('display').innerHTML = html; //render
    });
